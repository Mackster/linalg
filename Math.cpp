#include "Math.h"

std::string ConvertToString(float a, int precision)
{
	std::stringstream stream;
	stream << std::fixed << std::setprecision(2) << a;

	return stream.str();
}