#pragma once
#include <string>
#include <sstream>
#include <iomanip>

std::string ConvertToString(float a, int precision);