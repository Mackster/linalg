#pragma once
#include "Core.h"

class MathObject
{
private:
public:
	enum class Type
	{
		Number, Matrix
	};

	Type t;
	std::string CustomMessage;

	MathObject() : t(Type::Number) {}
	MathObject(const std::string& sMessage) : CustomMessage(sMessage), t(Type::Number) {}

	virtual std::string ToString() { return ""; };
};