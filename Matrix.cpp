#include "Matrix.h"

#include <iostream>

Matrix::Matrix(std::string input)
{
	t = MathObject::Type::Matrix;

	try
	{
		std::regex rowMatch("\\{([^\\{\\}]*?)\\}");

		auto start = std::sregex_iterator(input.begin(), input.end(), rowMatch);
		auto end = std::sregex_iterator();

		std::vector<std::string> matches;

		for (std::sregex_iterator i = start; i != end; i++)
		{
			matches.push_back((*i).str());
		}

		for (size_t i = 0; i < matches.size(); i++)
		{
			std::string str = matches[i];
			str = Erase(str, '{');
			str = Erase(str, '}');
			
			matches[i] = str;
		}

		for (size_t i = 0; i < matches.size(); i++)
		{
			std::string str = Erase(matches[i], ' ');
			std::vector<std::string> strnumbers = Split(matches[i], ',');

			std::vector<float> numbers;

			for (std::string s : strnumbers)
			{
				numbers.push_back(std::stof(s));
			}

			data.push_back(numbers);

			numbers = std::vector<float>();
		}
	}
	catch (const std::exception& e)
	{
		std::cout << "Error Creating Matrix: " << e.what() << std::endl;
	}
}

int Matrix::SizeX()
{
	return (int)data[0].size();
}

int Matrix::SizeY()
{
	return (int)data.size();
}

Matrix* Matrix::Rref()
{
	Matrix* m = new Matrix(*this);

	int lead = 0, rowCount = m->data.size(), columnCount = m->data[0].size();
	for (int r = 0; r < rowCount; r++)
	{
		if (columnCount <= lead) break;
		int i = r;

		while (m->data[i][lead] == 0)
		{
			i++;
			if (i == rowCount)
			{
				i = r;
				lead++;
				if (columnCount == lead)
				{
					lead--;
					break;
				}
			}
		}
		for (int j = 0; j < columnCount; j++)
		{
			float temp = m->data[r][j];
			m->data[r][j] = m->data[i][j];
			m->data[i][j] = temp;
		}

		float div = m->data[r][lead];

		if (div != 0)
		{
			for (int j = 0; j < columnCount; j++)
			{
				m->data[r][j] /= div;
			}
		}

		for (int j = 0; j < rowCount; j++)
		{
			if (j != r)
			{
				float sub = m->data[j][lead];
				for (int k = 0; k < columnCount; k++)
				{
					m->data[j][k] -= (sub * m->data[r][k]);
				}
			}
		}

		lead++;
	}

	for (size_t i = 0; i < rowCount; i++)
	{
		for (size_t j = 0; j < columnCount; j++)
		{
			if (m->data[i][j] == -0)
				m->data[i][j] = 0;
		}
	}

	return m;
}

Number* Matrix::Determinant()
{
	return new Number(Determinant(this->data));
}

float Matrix::Determinant(MatrixContents& a)
{
	if (a[0].size() == 1 && a.size() == 1)
	{
		return a[0][0];
	}

	int sign = 1;

	int total = 0;

	for (int i = 0; i < a.size(); i++)
	{
		std::vector<std::vector<float>> temp = Skip(a, i);

		total += sign * (a[i][0]) * Determinant(temp);

		sign *= -1;
	}

	return total;
}

MatrixContents Matrix::Skip(MatrixContents a, int idx)
{
	MatrixContents temp;

	for (size_t i = 0; i < a.size(); i++)
	{
		if (i != idx)
		{
			std::vector<float> row;

			for (size_t j = 1; j < a[i].size(); j++)
			{
				row.emplace_back(a[i][j]);
			}

			temp.emplace_back(row);
		}
	}

	return temp;
}

MatrixContents Cut(MatrixContents a, int idx)
{
	MatrixContents temp;

	if (idx > a.size())
		return temp;

	for (size_t i = 0; i < a.size(); i++)
	{
		std::vector<float> row;

		for (size_t j = 0; j < idx; j++)
		{
			row.emplace_back(a[i][j]);
		}

		temp.emplace_back(row);
	}

	return temp;
}

MatrixContents CutFrom(MatrixContents a, int idx)
{
	MatrixContents temp;

	if (idx > a.size())
		return temp;

	for (size_t i = 0; i < a.size(); i++)
	{
		std::vector<float> row;

		for (size_t j = idx; j < a[i].size(); j++)
		{
			row.emplace_back(a[i][j]);
		}

		temp.emplace_back(row);
	}

	return temp;
}

Matrix* Combine(Matrix* a, Matrix* b)
{
	if (a->SizeX() != b->SizeX() || a->SizeY() != b->SizeY())
		return (Matrix*)(new MathObject("Sizes don't match"));

	Matrix* temp = new Matrix(*a);

	for (size_t i = 0; i < temp->SizeY(); i++)
	{
		for (size_t j = 0; j < b->SizeX(); j++)
		{
			temp->data[i].emplace_back(b->data[i][j]);
		}
	}

	return temp;
}

Matrix* Inverse(Matrix* a)
{
	Matrix* tempId = CreateIdentity(a->SizeX());
	Matrix* tempCombine = Combine(a, tempId);

	Matrix* inv = tempCombine->Rref();

	inv->data = CutFrom(inv->data, inv->SizeY());

	delete tempId, tempCombine;

	return inv;
}

Matrix* CreateIdentity(int size)
{
	std::vector<std::vector<float>> data;

	for (size_t i = 0; i < size; i++)
	{
		std::vector<float> temp;

		for (size_t j = 0; j < size; j++)
		{
			temp.emplace_back(i == j ? 1 : 0);
		}
		
		data.emplace_back(temp);
	}

	return new Matrix(data);
}

std::string Matrix::ToString()
{
	std::string text = "   |";

	for (size_t i = 0; i < data.size(); i++)
	{
		int temp = 0;

		for (size_t j = 0; j < data[i].size() - 1; j++)
		{
			text += ConvertToString(data[i][j], 2) + " ";
			temp = j;
		}

		text += ConvertToString(data[i][temp + 1], 2) + "|";

		if (i < data.size() - 1)
			text += "\n   |";
	}

	return text;
}
