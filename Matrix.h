#pragma once
#include "MathObject.h"
#include "Math.h"
#include "Util.h"

#include "Number.h"

typedef std::vector<std::vector<float>> MatrixContents;

class Matrix : public MathObject
{
private:
	float Determinant(MatrixContents& a);
	MatrixContents Skip(MatrixContents a, int idx);

public:
	MatrixContents data;

	Matrix(std::string input);

	Matrix(MatrixContents _data) : data(_data) { t = MathObject::Type::Matrix; }

	int SizeX();
	int SizeY();

	Matrix* Rref();
	Number* Determinant();
	

	std::string ToString() override;
};

Matrix* CreateIdentity(int size);
Matrix* Combine(Matrix* a, Matrix* b);
Matrix* Inverse(Matrix* a);
MatrixContents Cut(MatrixContents a, int idx);
MatrixContents CutFrom(MatrixContents a, int idx);