#pragma once
#include "MathObject.h"
#include "Math.h"
#include "Util.h"

class Number : public MathObject
{
public:
	float data;

	Number(float n) : data(n) { t = MathObject::Type::Number; }

	std::string ToString() override;
};