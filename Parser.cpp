#include "Parser.h"

ObjectMap variables;

void Assignment(const std::string& text)
{
	std::vector<std::string> parts = SplitTwo(text, '=');
	std::string name = parts[0];

	std::string value = parts[1];

	variables[name] = Parse(value);
}

void Cleanup()
{
	for (auto idx : variables)
	{
		delete idx.second;
	}
}

MathObject* CreateMatrix(std::string& data)
{
	return new Matrix(Erase(data, "Matrix "));
}

MathObject* Size(std::string& data)
{
	std::string temp = Erase(data, "size ");
	Matrix* m = (Matrix*)Parse(temp);

	std::string text = std::to_string(m->SizeX()) + "x" + std::to_string(m->SizeY());

	return new MathObject(text);
}

MathObject* Combine(std::string& data)
{
	std::string temp = Erase(data, "combine ");

	std::vector<std::string> arguments = SplitTwo(temp, ' ');

	Matrix* a = (Matrix*)Parse(arguments[0]);
	Matrix* b = (Matrix*)Parse(arguments[1]);

	return Combine(a, b);
}

MathObject* Parse(std::string& input)
{
	std::vector<std::string> parts = Split(input);

	if (Contains(variables, input))
		return variables[input];

	if(Contains(input, '='))
	{
		Assignment(input);

		return new MathObject;
	}
	else if (Contains(input, "Matrix "))
	{
		return CreateMatrix(input);
	}
	else if (Contains(input, "rref"))
	{
		return Rref(input);
	}
	else if (Contains(input, "id"))
	{
		int size = std::stoi(Erase(input, "id "));
		return CreateIdentity(size);
	}
	else if (Contains(input, "size"))
	{
		return Size(input);
	}
	else if (Contains(input, "combine"))
	{
		return Combine(input);
	}
	else if (Contains(input, "det"))
	{
		return Determinant(input);
	}
	else if (Contains(input, "inv"))
	{
		return Inverse(input);
	}

	return new MathObject;
}

MathObject* Rref(std::string& input)
{
	std::string data = Erase(input, "rref ");
	MathObject* temp = Parse(data);

	if (temp->t != MathObject::Type::Matrix)
		return new MathObject;

	return ((Matrix*)(temp))->Rref();
}

MathObject* Determinant(std::string& data)
{
	std::string value = Erase(data, "det ");
	Matrix* temp = (Matrix*)Parse(value);

	Number* det = temp->Determinant();

	if (!Contains(variables, temp))
		delete temp;

	return det;
}

MathObject* Inverse(std::string& data)
{
	std::string value = Erase(data, "inv ");
	Matrix* temp = (Matrix*)Parse(value);

	Matrix* inv = Inverse(temp);

	if (!Contains(variables, temp))
		delete temp;

	return inv;
}
