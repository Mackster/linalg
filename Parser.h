#pragma once

#include "Core.h"
#include "Util.h"

#include "Matrix.h"

void Assignment(const std::string& text);
void Cleanup();

MathObject* Parse(std::string& input);

MathObject* CreateMatrix(std::string& data);
MathObject* Size(std::string& data);
MathObject* Combine(std::string& data);
MathObject* Rref(std::string& data);
MathObject* Determinant(std::string& data);
MathObject* Inverse(std::string& data);