#include "Terminal.h"

void Terminal::PrintLn(std::string text)
{
	std::cout << text << std::endl;
}

void Terminal::Print(std::string text)
{
	std::cout << text;
}

std::string Terminal::Read()
{
	Print(prompt);

	std::string input = "";

	std::getline(std::cin, input);

	return input;
}

void Terminal::Run()
{
	while (running)
	{
		std::string input = Read();

		if (input == "#e")
			running = false;
		else if (input == "#c")
			CLEAR;
		else
		{
			MathObject* result = Parse(input);

			std::string str = result->ToString();

			if (str != "")
				PrintLn(str);
			else if (result->CustomMessage != "")
				PrintLn(result->CustomMessage);

			if(!Contains(variables, result))
				delete result;
		}
	}

	Cleanup();
}
