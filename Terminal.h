#pragma once

#include <iostream>
#include "Core.h"

#include "Parser.h"
#include "Util.h"

#ifdef _WIN32
#define CLEAR system("cls")
#else
#define CLEAR system("clear")
#endif

extern ObjectMap variables;

class Terminal
{
private:
    std::string prompt = ">";

	void Print(std::string text);
	void PrintLn(std::string text);
	std::string Read();

public:
    bool running = true;

	void Run();
};