#include "Util.h"

const std::string WHITESPACE = " \n\r\t\f\v";

std::vector<std::string> SplitTwo(const std::string& text, char k)
{
	std::vector<std::string> parts = Split(text, k);

	std::string temp = "";

	size_t size = parts.size() - 1;

	for (size_t i = 1; i < size; i++)
	{
		temp += parts[i] + k;
	}

	temp += parts[size];

	return { parts[0], temp };
}

std::vector<std::string> Split(const std::string& text, char k)
{
	std::vector<std::string> parts;

	std::string part = "";

	for (char c : text)
	{
		if (c == k)
		{
			part = Trim(part);
			parts.emplace_back(part);
			part = "";
		}
		else
		{
			part += c;
		}
	}

	if (part.size() != 0)
	{
		part = Trim(part);
		parts.emplace_back(part);
	}
	return parts;
}

std::vector<std::string> Split(const std::string& s)
{
	return Split(s, ' ');
}

bool Contains(const std::string& text, char c)
{
	for (char k : text)
	{
		if (k == c)
			return true;
	}

	return false;
}

bool Contains(const std::string& text, std::string sub)
{
	return text.find(sub) != std::string::npos;
}

bool Contains(ObjectMap& map, std::string key)
{
	ObjectMap::iterator it = map.find(key);

	if (it != map.end())
	{
		return true;
	}

	return false;
}

bool Contains(ObjectMap& map, MathObject* data)
{
	for (ObjectMap::iterator it = map.begin(); it != map.end(); it++)
	{
		if (it->second == data)
			return true;
	}

	return false;
}

std::string Ltrim(const std::string& s)
{
	size_t start = s.find_first_not_of(WHITESPACE);
	return (start == std::string::npos) ? "" : s.substr(start);
}

std::string Rtrim(const std::string& s)
{
	size_t end = s.find_last_not_of(WHITESPACE);
	return (end == std::string::npos) ? "" : s.substr(0, end + 1);
}

std::string Trim(const std::string& s)
{
	return Rtrim(Ltrim(s));
}

std::string Erase(std::string& mainStr, const std::string& toErase)
{
	size_t pos = mainStr.find(toErase);

	if (pos != std::string::npos)
	{
		mainStr.erase(pos, toErase.length());
	}

	return mainStr;
}

std::string Erase(std::string& mainStr, const char c)
{
	mainStr.erase(std::remove(mainStr.begin(), mainStr.end(), c), mainStr.end());

	return mainStr;
}
