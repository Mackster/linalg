#pragma once

#include "Core.h"
#include "MathObject.h"

#include <algorithm>
#include <regex>

typedef std::map<std::string, MathObject*> ObjectMap;

std::vector<std::string> Split(const std::string& text, char c);
std::vector<std::string> Split(const std::string& s);
std::vector<std::string> SplitTwo(const std::string& text, char c);

bool Contains(const std::string& text, char c);
bool Contains(const std::string& text, std::string sub);
bool Contains(ObjectMap& map, std::string key);
bool Contains(ObjectMap& map, MathObject* data);

std::string Ltrim(const std::string& s);
std::string Rtrim(const std::string& s);
std::string Trim(const std::string& s);

std::string Erase(std::string& mainStr, const std::string& toErase);
std::string Erase(std::string& mainStr, const char c);