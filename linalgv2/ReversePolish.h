#pragma once
#include "Core.h"
#include "Token.h"
#include "Stack.h"
#include "Interpreter.h"

std::map<std::string, int> precedence = { {"=", 1}, {"+", 2}, {"-", 2}, {"*", 3}, {"/", 3} };

Stack opStack;
Stack s;

std::vector<MathObject> ToPolishForm(std::vector<Token> tokens)
{
	for (size_t i = 0; i < tokens.size(); i++)
	{
		Token t = tokens[i];

		TokenType type = t.GetType();

		if (type == TokenType::Num)
		{
			Number n(std::stof(t.GetValue()));
			s.Push(n);
		}
		else if (type == TokenType::Matrix)
		{
			Matrix mat(t.GetValue());
			s.Push(mat);
		}
		else if (type == TokenType::Func)
		{
			Function f(&Add, t.GetValue());

			opStack.Push(f);
		}
		else if (type == TokenType::Operator)
		{
			int pre = precedence[t.GetValue()];


		}
	}
}