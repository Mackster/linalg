#pragma once
#include "Core.h"
#include "Token.h"

class Stack
{
	std::vector<MathObject> data;

public:

	Stack() {};

	void Push(MathObject o)
	{
		data.emplace_back(o);
	}

	MathObject Pop()
	{
		if (data.size() < 1)
			return;

		MathObject temp = data.back();
		data.pop_back();

		return temp;
	}

	std::vector<MathObject> GetData()
	{
		return data;
	}

	int GetPrecedenceOfLast(std::map<std::string, int>& precedence)
	{
		if (data.size() < 1)
			return - 1;
	}
};