#include "Terminal.h"

void Terminal::PrintLn(const std::string& text)
{
	std::cout << text << std::endl;
}

void Terminal::Print(const std::string& text)
{
	std::cout << text;
}

std::string Terminal::Read()
{
	Print(prompt);

	std::string input = "";

	std::getline(std::cin, input);

	return input;
}

void Terminal::Run()
{
	while (running)
	{
		std::string input = Read();

		std::vector<Token> result = Tokenize(input);

		for (size_t i = 0; i < result.size(); i++)
		{
			std::cout << result[i].ToString() << std::endl;
		}
	}
}
