#pragma once

#include <iostream>
#include "Core.h"

#include "Tokenizer.h"

#ifdef _WIN32
#define CLEAR system("cls")
#else
#define CLEAR system("clear")
#endif

class Terminal
{
private:
    std::string prompt = ">";

	void Print(const std::string& text);
	void PrintLn(const std::string& text);
	std::string Read();

public:
    bool running = true;

	void Run();
};