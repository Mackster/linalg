#pragma once
#include "Core.h"

enum class TokenType { Num, Matrix, Operator, Func, Var, None};

class Token
{
private:
	std::string value;
	TokenType t;

	std::string TypeStrings[6] = { "Num", "Matrix", "Operator", "Func", "Var", "None"};

public:
	Token() : t(TokenType::None), value("") {}
	Token(const std::string& _value, const TokenType _t) : value(_value), t(_t) {}
	Token(char _value, const TokenType _t) : value(1, _value), t(_t) {}
	~Token() {};

	std::string ToString()
	{
		return "(" + value + ", " + TypeStrings[(int)t] + ")";
	}

	TokenType GetType()
	{
		return t;
	}

	std::string GetValue()
	{
		return value;
	}
};

class MathObject
{
public:
	TokenType t;

	MathObject(const TokenType _t) : t(_t) {};

	virtual std::string ToString() { return ""; }
	virtual std::string Type() { return ""; }
};

class Matrix : public MathObject
{
public:
	MatrixData data;

	Matrix(MatrixData _data) : data(_data), MathObject(TokenType::Matrix) {}
	Matrix(const std::string& _data) : MathObject(TokenType::Matrix) {}

	std::string ToString() override
	{
		std::string text = "   |";

		for (size_t i = 0; i < data.size(); i++)
		{
			size_t temp = 0;

			for (size_t j = 0; j < data[i].size() - 1; j++)
			{
				text += ConvertToString(data[i][j], 2) + " ";
				temp = j;
			}

			text += ConvertToString(data[i][temp + 1], 2) + "|";

			if (i < data.size() - 1)
				text += "\n   |";
		}

		return text;
	}

	std::string Type() override
	{
		return "MathObject::Matrix";
	}
};

class Number : public MathObject
{
public:
	float data;

	Number(const float _data) : data(_data), MathObject(TokenType::Num) {};

	std::string ToString() override
	{
		return ConvertToString(data, 2);
	}

	std::string Type() override
	{
		return "MathObject::Float";
	}
};

class Function : public MathObject
{
public:
	typedef MathObject(*Fun)(MathObject&, MathObject&);
	Fun f;
	std::string name;

	Function(Fun _f, std::string _name) : f(_f), name(_name), MathObject(TokenType::Func) {};

	std::string ToString() override
	{
		return name;
	}

	std::string Type() override
	{
		return "MathObject::Function (MathObject, MathObject) -> MathObject";
	}
};