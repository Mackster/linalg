#include "Tokenizer.h"

#define DONE -0x1A4

size_t idx = -1;
char current;

std::string operators = "()+-*/=";
std::string digits = "0123456789";

#define FUN_SIZE 4
std::string functions[FUN_SIZE] = { "rref", "id", "inv", "det" };

Token MakeNumber(const std::string& text);
Token MakeMatrix(const std::string& text);
Token MakeWord(const std::string& text);

Token tPrev;
char cPrev;
char cNext;

void Advance(int amount, const std::string& text)
{
    idx += amount;

    if(idx >= text.size())
    {
        idx = DONE;
        return;
    }

	if (idx < text.size() - 1)
	{
		cNext = text[idx + 1];
	}

    if(idx < 0)
        idx = 0;

	cPrev = current;
    current = text[idx];
}

std::vector<Token> Tokenize(const std::string& text)
{
    idx = -1;
    Advance(1, text);

    std::vector<Token> Tokenized;

	while (idx != DONE)
	{
		if (Tokenized.size() > 0)
			tPrev = Tokenized[Tokenized.size() - 1];

		while (current == ' ' && idx != DONE)
		{
			Advance(1, text);
		}

		if (idx == DONE)
			break;

		bool found = false;

		for (char c : operators)
		{
			if (c == current)
			{
				if (tPrev.GetType() != TokenType::Operator && tPrev.GetType() != TokenType::None)
				{
					Tokenized.emplace_back(Token(c, TokenType::Operator));
					found = true;
				}
			}
		}

		if (!found)
		{
			if (Contains(digits, current) || (current == '-' && Contains(digits, cNext)))
			{
				Tokenized.emplace_back(MakeNumber(text));
			}
			else if (current == '{')
			{
				Tokenized.emplace_back(MakeMatrix(text));
			}
			else
			{
				Tokenized.emplace_back(MakeWord(text));
			}
		}

		Advance(1, text);
	}

	return Tokenized;
}

Token MakeNumber(const std::string& text)
{
	int points = 0;
	int min = 0;

	std::string number;

	if (cPrev == '-' && tPrev.GetType() != TokenType::Operator)
	{
		number += '-';
	}

	while (Contains(digits + ".-", current) && idx != DONE)
	{
		if (current == '.')
			points++;

		if (current == '-')
			min++;

		if (points > 1 || min > 1)
			break;

		number += current;
		Advance(1, text);
	}

	Advance(-1, text);

	return Token(number, TokenType::Num);
}

Token MakeMatrix(const std::string& text)
{
	std::string sMatrix = "{";

	int count = 1;

	Advance(1, text);

	while (count != 0 && idx != DONE)
	{
		if (current == '{')
			count++;

		if (current == '}')
			count--;

		sMatrix += current;

		Advance(1, text);
	}

	return Token(sMatrix, TokenType::Matrix);
}

Token MakeWord(const std::string& text)
{
	std::string value;

	while (current != ' ' && idx != DONE)
	{
		value += current;
		Advance(1, text);
	}

	Advance(-1, text);

	bool found = false;

	for (size_t i = 0; i < FUN_SIZE; i++)
	{
		if (functions[i] == value)
			found = true;
	}

	if(found)
		return Token(value, TokenType::Func);
	else
		return Token(value, TokenType::Var);
}