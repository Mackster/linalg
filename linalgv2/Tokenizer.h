#pragma once

#include "Core.h"
#include "Util.h"
#include "Token.h"

std::vector<Token> Tokenize(const std::string& text);