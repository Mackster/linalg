#include "Util.h"

bool Contains(const std::string& text, char c)
{
    for (char k : text)
    {
        if(k == c)
            return true;
    }

    return false;
}

std::string ConvertToString(float a, int precision)
{
    std::stringstream stream;
    stream << std::fixed << std::setprecision(2) << a;

    return stream.str();
}