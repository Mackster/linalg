#pragma once

#include "Core.h"
#include <sstream>
#include <iomanip>

bool Contains(const std::string& text, char c);
std::string ConvertToString(float a, int precision);