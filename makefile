FILES = *.cpp
OUT_NAME = linalg.out
BINARY_NAME = linalg
default: compile_wdbg

clean:
	rm $(OUT_NAME) && sudo rm -f /usr/bin/$(BINARY_NAME)
install: compile_woutdbg
	sudo cp -f $(OUT_NAME) /usr/bin/$(BINARY_NAME)
compile_wdbg:
	g++ -g $(FILES) -o $(OUT_NAME)
compile_woutdbg:
	g++ $(FILES) -o $(OUT_NAME)
